const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(session({
    secret: 'this-is-a-secret-token',
    cookie: { maxAge: 60000 },
    resave: true,
    saveUninitialized: true
}));


app.post("/form", (req,res)=> {
    if(!req.body) return res.sendStatus(400);

    res.render('thanks.ejs', {
        name: req.body.name
        }
        )
});



app.listen(3001, ()=>{
    console.log("listening on port 3001")
});

