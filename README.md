# SpareRoom Single Event Page

The task is to build a single page showing an event, within the page is a form to submit details for a ticket as well as information on the event and the location.


##  Technology used

- React JS with the [create-react-app](https://github.com/facebook/create-react-app) provided by Facebook
- [Express](https://expressjs.com/) for a very little feature that handles the post request from the submitted form
- Jest and Enzyme are the testing utilities used to run tests on the state of the App states.



### installing and running the app

To install the dependencies install, from your terminal:

```
git clone https://pablobitbucket123@bitbucket.org/pablobitbucket123/spareroom.git
```

Then please install the node packages with 
the following command (from the spareroom folder)

```
cd spareroom
npm install 

```

Then the app can be run with : 

```
npm start
```

Your local browser http://localhost:3000/ 
will be running the app. 

An express back-end has been added which needs to be run 
from a new Terminal window : 

```
node app
```



## Running the tests

Use of jest and enzyme for React. 

We are using jest as the test runner and 
we are using enzyme functions to test the state 
of the App component, which is our one and only component. 

The test can be run by typing : 

```
npm test
```

Then press the `a` key in order to run the tests. 

The very first test takes a snapshot of the 
page, and it is bound to fail every time we add something different
to the App.js component. So if we add for 
example an additional word to an h1 header, the snapshot will fail 
immediately. All we need to do is press the `u` key so the 
snapshot gets updated with the new version of the page. It is done 
automatically. 

## What is missing 

- I am not checking the format of the data submitted via the form, which 
should be a critical security measure. 
- I set up an express app, but  I could get only the name and the email address
of the user, so it just redirects the user to a "thank you" page who says her/his 
name. 
- I have built a big App.js component without breaking it down   
 into several small components. I am missing on the best part of ReactJS 
whereby we can decouple each part of the frontend layout 
into smaller components. Those small components can then be used 
elsewhere in the app
- I am displaying the Google Map with the google map API by using directly the 
index.html file. I was not able to build a React component, which would have been ideal. 
- I have tried to follow the specific desgin guidelines provided, 
however it probably does not match all the required lengths and widths. 
- I have built the app with a specific focus on mobile screens. 
I wanted to shift my focus to the desktop version afterwards. However, I 
realise that I have many changes to make in order to have a better desktop version:
the logo for example is a bit cut off on the desktop version. 
 






