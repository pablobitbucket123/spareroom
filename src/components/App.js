import React, {Component} from 'react';
import '../index.css';



class App extends Component {
    constructor(){
        super();

        this.state = {
            name: "",
            email: "",
            defaultCheckedOne: false,
            defaultCheckedTwo: false
        }
    }

    handleChangeName = (event) => {
        this.setState({
            name: event.target.value
                 });
    }

    handleChangeEmail = (event) => {
        this.setState({
            email: event.target.value
        });
    }

    handleClickRadioButtonOne = (event) => {
        this.setState({
            defaultCheckedOne : true,
            defaultCheckedTwo: false
        })
    }

    handleClickRadioButtonTwo = (event) => {
        this.setState({
            defaultCheckedOne : false,
            defaultCheckedTwo: true
        })
    }

    render() {
        const ColoredLine = ({ color }) => (
            <hr
                style={{
                    color: color,
                    backgroundColor: color,
                    height: 1,
                    paddingTop: 0,
                    marginBottom: 10,
                    marginTop: 10
                }}
            />
        );


        return (
            <div className='main-flex-container'>
                <div className='main-flex-item'>
                    <div id='spareroom-logo' ></div>
                <nav>
                    <a href='/html/'>Home</a>
                    <a href='/css/'>Events</a>
                </nav>
            </div>
                <div className='name-premises main-flex-item' >
                    SpeedRoomMating Brooklyn
                </div>

                <div className='main-flex-item container'>

                    <img className='event-image' src={require('../assets/small-bar.png')} alt='bar event' />
                    <div className='top-left' >
                        <p className='name-event-place' >Matt Torrey's</p>
                        <p className='day-event' >Tuesday 27th June</p>
                        <p className='time-event'>7:00 PM - 9:00 PM </p>
                        <a className='directions-link-event' href='#map' > Venue's Map & directions </a>
                    </div>
                    <ColoredLine color='#D5D5D5'/>
                </div>

                <div className='main-flex-item'>
                    <div className='form-header'> Reserve your place now </div>
                    <form action='/form' method='post'>
                        <div className='form-radio'>
                            <input type='radio' value='need-room' checked={this.state.defaultCheckedOne} onClick={this.handleClickRadioButtonOne}/> I need a room

                            <input type='radio' value='have-room' checked={this.state.defaultCheckedTwo} onClick={this.handleClickRadioButtonTwo}/> I have a room

                        </div>

                        <div className='form-name'>
                            <label>
                                Name:
                                <input type='text' name="name"
                                       value={this.state.name}
                                       onChange={this.handleChangeName}
                                       placeholder="Please enter your name"
                                        className='input-name'
                                       id='input-name'
                                />
                            </label>
                            <label>
                                Email:
                                <input type='text'
                                       name="email"
                                       value={this.state.email}
                                       onChange={this.handleChangeEmail}
                                       className='input-email'
                                       placeholder="Please enter your email"

                                />
                            </label>
                        </div>


                        <div>
                            <p className='privacy-policy'> We won't give your email address to anyone else</p>
                            <p className='privacy-policy privacy-link' > See our <a> Privacy Policy</a> for more details</p>
                        </div>

                        <input id='btn-orange' type="submit" value="Book"/>
                    </form>
                </div>

                <div className='main-flex-item'>
                    <ColoredLine color='#D5D5D5'/>

                    <div className='header-question'> What areas does this event cover ?</div>
                    <div className='header-question'>Venue address </div>
                    <p> 46 Bushwick Ave, Ainslie Street</p>
                    <p> Brooklyn, NY 11211</p>
                    <div className='header-question'>Venue directions</div>
                    <p>L train to Grand Avenue. Walk
                    2 blocks north on Bushwick Avenue,
                        bar is on the left on the corner
                        of Bushwick and Ainslie
                    </p>
                    <div className='header-question' >Nearest subway</div>
                    <p>Grand Street (L train)
                    </p>
                    <div className='header-question'> Map </div>
                    <div id='map'>  </div>
                </div>

            </div>
        )
    }


}

export default App;