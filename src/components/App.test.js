import React from 'react'
import {shallow} from 'enzyme'
import App from './App'

describe ('App', ()=>{
    const app = shallow(<App/>);

    it ('renders correctly', ()=>{
        expect(app).toMatchSnapshot();
    });

    it('initialises the state with an empty name', ()=>{
        expect(app.state().name).toEqual("");
    });

    describe('when writing on the name/email form',()=>{
       const userName = 'John';


       beforeEach(()=>{
           app.find('input.input-name').simulate('change', {
               target: { value: userName}
           });


       });

        it('changes the name state ', ()=>{
            expect(app.state().name).toEqual(userName);
       });

    });

    describe('when writing on the name/email form',()=>{
        const emailAdress = 'john@email.com'

        beforeEach(()=>{

            app.find('input.input-email').simulate('change', {
                target: { value: emailAdress}
            });
        });


        it('changes the email state', ()=>{
            expect(app.state().email).toEqual(emailAdress);
        });
    });


})